package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// user represents data about a record user.
type user struct {
	ID       string `json:"id"`
	Username string `json:"username"`
	Age      int    `json:"age"`
}

var users = []user{
	{ID: "1", Username: "PcYou367", Age: 25},
	{ID: "2", Username: "Pirex367", Age: 26},
	{ID: "3", Username: "hr98hr", Age: 25},
}

// getusers responds with the list of all users as JSON.
func getUsers(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, users)
}

func addUser(c *gin.Context) {
	var newUser user

	// Call BindJSON to bind the received JSON to
	// newuser.
	if err := c.BindJSON(&newUser); err != nil {
		return
	}

	// Add the new user to the slice.
	users = append(users, newUser)
	c.IndentedJSON(http.StatusCreated, newUser)
}

func getUserById(c *gin.Context) {
	id := c.Param("id")
	// Loop over the list of users, looking for
	// an user whose ID value matches the parameter.
	for _, a := range users {
		if a.ID == id {
			c.IndentedJSON(http.StatusOK, a)
			return
		}
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": fmt.Sprintf("user %s not found", id)})

}

func deleteUserById(c *gin.Context) {
	id := c.Param("id")
	// Loop over the list of users, looking for
	// an user whose ID value matches the parameter.
	for i, a := range users {
		if a.ID == id {
			c.JSON(http.StatusNoContent, "")
			return
		}
		users = append(users[:i], users[i+1:]...)
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": fmt.Sprintf("user %s not found", id)})
}

func editUser(c *gin.Context) {
	var updatedUser user

	// Call BindJSON to bind the received JSON to
	// newuser.
	if err := c.BindJSON(&updatedUser); err != nil {
		return
	}

	id := updatedUser.ID
	// Loop over the list of users, looking for
	// an user whose ID value matches the parameter.
	for i, a := range users {
		if a.ID == id {
			c.JSON(http.StatusOK, a)
			return
		}
		users[i] = updatedUser
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": fmt.Sprintf("user %s not found", id)})
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func readinessHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func main() {

	router := gin.Default()
	router.GET("/users", getUsers)
	router.GET("/users/:id", getUserById)
	router.POST("/users", addUser)
	router.PATCH("/users", editUser)
	router.DELETE("/users/:id", deleteUserById)
	router.Run("localhost:8080")
}
