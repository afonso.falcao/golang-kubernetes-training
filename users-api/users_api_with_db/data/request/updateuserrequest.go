package datarequest

type UpdateUserRequest struct {
	ID       int    `validate:"required"`
	Username string `validate:"required,min=1,max=200" json:"username"`
	Age      int    `validate:"required" json:"age"`
}
