package main

import (
	"go_api_with_db/controller"
	"go_api_with_db/model"
	"go_api_with_db/repository"
	"go_api_with_db/router"
	"go_api_with_db/service"
	"go_api_with_db/utils"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/rs/zerolog/log"
)

// @title 	Tag Service API
// @version	1.0
// @description A Tag service API in Go using Gin framework

// @host 	localhost:8888
// @BasePath /api
func main() {

	log.Info().Msg("Started Server!")
	// Database
	db := repository.DatabaseConnection()
	validate := validator.New()

	db.Table("users").AutoMigrate(&model.User{})

	// Repository
	userRepository := repository.NewUserRepositoryImpl(db)

	// Service
	userService := service.NewUserServiceImpl(userRepository, validate)

	// Controller
	userController := controller.NewUserController(userService)

	// Router
	routes := router.NewRouter(userController)

	server := &http.Server{
		Addr:    ":8080",
		Handler: routes,
	}

	err := server.ListenAndServe()
	utils.ErrorPanic(err)
}
