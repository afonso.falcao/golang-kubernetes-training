package service

import (
	datarequest "go_api_with_db/data/request"
	dataresponse "go_api_with_db/data/response"
	"go_api_with_db/model"
	"go_api_with_db/repository"
	"go_api_with_db/utils"

	"github.com/go-playground/validator/v10"
)

type UserServiceImpl struct {
	UserRepository repository.UserRepository
	Validate       *validator.Validate
}

func NewUserServiceImpl(userRepository repository.UserRepository, validate *validator.Validate) UserService {
	return &UserServiceImpl{
		UserRepository: userRepository,
		Validate:       validate,
	}
}

// Create implements UserService
func (t *UserServiceImpl) Create(user datarequest.CreateUserRequest) {
	err := t.Validate.Struct(user)
	utils.ErrorPanic(err)
	userModel := model.User{
		Username: user.Username,
		Age:      user.Age,
	}
	t.UserRepository.Save(userModel)
}

// Delete implements UserService
func (t *UserServiceImpl) Delete(userId int) {
	t.UserRepository.Delete(userId)
}

// FindAll implements UserService
func (t *UserServiceImpl) FindAll() []dataresponse.UserResponse {
	result := t.UserRepository.FindAll()

	var user []dataresponse.UserResponse
	for _, value := range result {
		user1 := dataresponse.UserResponse{
			ID:       value.ID,
			Username: value.Username,
		}
		user = append(user, user1)
	}

	return user
}

// FindById implements UserService
func (t *UserServiceImpl) FindById(userId int) dataresponse.UserResponse {
	userData, err := t.UserRepository.FindById(userId)
	utils.ErrorPanic(err)

	userResponse := dataresponse.UserResponse{
		ID:       userData.ID,
		Username: userData.Username,
	}
	return userResponse
}

// Update implements UserService
func (t *UserServiceImpl) Edit(user datarequest.UpdateUserRequest) {
	userData, err := t.UserRepository.FindById(user.ID)
	utils.ErrorPanic(err)
	userData.Username = user.Username
	t.UserRepository.Edit(userData)
}
