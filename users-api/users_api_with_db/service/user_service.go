package service

import (
	datarequest "go_api_with_db/data/request"
	dataresponse "go_api_with_db/data/response"
)

type UserService interface {
	Create(user datarequest.CreateUserRequest)
	Edit(user datarequest.UpdateUserRequest)
	Delete(userId int)
	FindById(userId int) dataresponse.UserResponse
	FindAll() []dataresponse.UserResponse
}
