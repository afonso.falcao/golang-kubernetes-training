package repository

import (
	"go_api_with_db/model"
)

type UserRepository interface {
	Save(users model.User)
	Edit(users model.User) (user model.User)
	Delete(userId int)
	FindById(userId int) (users model.User, err error)
	FindAll() []model.User
}
