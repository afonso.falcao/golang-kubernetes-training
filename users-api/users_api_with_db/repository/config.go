package repository

import (
	"fmt"
	"os"
	"strconv"

	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type DBInfo struct {
	host     string
	port     int
	user     string
	password string
	dbName   string
}

func DatabaseConnection() *gorm.DB {
	// Setup env vars
	err := godotenv.Load()
	info := DBInfo{
		host:     "users-api-postgres",
		port:     5432,
		user:     "postgres",
		password: "postgres",
		dbName:   "test",
	}

	if err == nil {
		log.Info().Msg("No configurations found, using default values")
		p, e := strconv.Atoi(os.Getenv("DB_PORT"))
		if e != nil {
			panic(e)
		}
		info = DBInfo{
			host:     os.Getenv("DB_HOST"),
			port:     p,
			user:     os.Getenv("DB_USER"),
			password: os.Getenv("DB_PASSWORD"),
			dbName:   os.Getenv("DB_NAME"),
		}
	}

	sqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", info.host, info.port, info.user, info.password, info.dbName)

	db, err := gorm.Open(postgres.Open(sqlInfo), &gorm.Config{})

	if err != nil {
		panic(err)
	}

	return db
}
