package repository

import (
	"errors"
	datarequest "go_api_with_db/data/request"
	"go_api_with_db/model"
	"go_api_with_db/utils"

	"gorm.io/gorm"
)

type UserRepositoryImpl struct {
	Db *gorm.DB
}

func NewUserRepositoryImpl(Db *gorm.DB) UserRepository {
	return &UserRepositoryImpl{Db: Db}
}

func (t UserRepositoryImpl) Save(user model.User) {
	result := t.Db.Create(&user)
	utils.ErrorPanic(result.Error)

}

func (t UserRepositoryImpl) Edit(user model.User) model.User {
	var updateTag = datarequest.UpdateUserRequest{
		ID:       user.ID,
		Username: user.Username,
	}
	result := t.Db.Model(&user).Updates(updateTag)
	utils.ErrorPanic(result.Error)
	return user
}

func (t UserRepositoryImpl) Delete(userId int) {
	var user model.User
	result := t.Db.Where("id = ?", userId).Delete(&user)
	utils.ErrorPanic(result.Error)
}

func (t UserRepositoryImpl) FindById(userId int) (model.User, error) {
	var user model.User
	result := t.Db.Find(&user, userId)
	if result != nil {
		return user, nil
	} else {
		return user, errors.New("user is not found")
	}
}

func (t UserRepositoryImpl) FindAll() []model.User {
	var user []model.User
	results := t.Db.Find(&user)
	utils.ErrorPanic(results.Error)
	return user
}
