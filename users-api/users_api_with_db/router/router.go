package router

import (
	"go_api_with_db/controller"

	"net/http"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func NewRouter(userController *controller.UserController) *gin.Engine {
	router := gin.Default()
	// add swagger
	router.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	router.GET("", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, "welcome home")
	})
	baseRouter := router.Group("/api")
	userRouter := baseRouter.Group("/users")
	userRouter.GET("", userController.FindAll)
	userRouter.GET("/:id", userController.FindById)
	userRouter.POST("", userController.Create)
	userRouter.PATCH("/:id", userController.Update)
	userRouter.DELETE("/:id", userController.Delete)

	return router
}
