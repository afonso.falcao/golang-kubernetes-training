package controller

import (
	datarequest "go_api_with_db/data/request"
	"go_api_with_db/service"
	"go_api_with_db/utils"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

type UserController struct {
	userService service.UserService
}

func NewUserController(service service.UserService) *UserController {
	return &UserController{
		userService: service,
	}
}

func (controller *UserController) Create(ctx *gin.Context) {
	log.Info().Msg("create user")
	createUserRequest := datarequest.CreateUserRequest{}
	err := ctx.ShouldBindJSON(&createUserRequest)
	utils.ErrorPanic(err)

	controller.userService.Create(createUserRequest)
	webResponse := utils.Response{
		Code:   http.StatusCreated,
		Status: "Ok",
		Data:   nil,
	}
	ctx.Header("Content-Type", "application/json")
	ctx.JSON(http.StatusOK, webResponse)
}

func (controller *UserController) Update(ctx *gin.Context) {
	log.Info().Msg("update user")
	updateUserRequest := datarequest.UpdateUserRequest{}
	err := ctx.ShouldBindJSON(&updateUserRequest)
	utils.ErrorPanic(err)

	id := ctx.Param("id")
	id2, err := strconv.Atoi(id)
	utils.ErrorPanic(err)
	updateUserRequest.ID = id2

	controller.userService.Edit(updateUserRequest)

	webResponse := utils.Response{
		Code:   http.StatusOK,
		Status: "Ok",
		Data:   nil,
	}
	ctx.Header("Content-Type", "application/json")
	ctx.JSON(http.StatusOK, webResponse)
}

func (controller *UserController) Delete(ctx *gin.Context) {
	log.Info().Msg("delete user")
	id := ctx.Param("id")
	id2, err := strconv.Atoi(id)
	utils.ErrorPanic(err)
	controller.userService.Delete(id2)

	webResponse := utils.Response{
		Code:   http.StatusOK,
		Status: "Ok",
		Data:   nil,
	}
	ctx.Header("Content-Type", "application/json")
	ctx.JSON(http.StatusOK, webResponse)
}

func (controller *UserController) FindById(ctx *gin.Context) {
	log.Info().Msg("findbyid user")
	id := ctx.Param("id")
	id2, err := strconv.Atoi(id)
	utils.ErrorPanic(err)

	userResponse := controller.userService.FindById(id2)

	webResponse := utils.Response{
		Code:   http.StatusOK,
		Status: "Ok",
		Data:   userResponse,
	}
	ctx.Header("Content-Type", "application/json")
	ctx.JSON(http.StatusOK, webResponse)
}

func (controller *UserController) FindAll(ctx *gin.Context) {
	log.Info().Msg("findAll user")
	userResponse := controller.userService.FindAll()
	webResponse := utils.Response{
		Code:   http.StatusOK,
		Status: "Ok",
		Data:   userResponse,
	}
	ctx.Header("Content-Type", "application/json")
	ctx.JSON(http.StatusOK, webResponse)

}
