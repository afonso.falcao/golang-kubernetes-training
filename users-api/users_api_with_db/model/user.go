package model

type User struct {
	ID       int    `gorm:"type:int;primary_key;AUTO_INCREMENT"`
	Username string `gorm:"type:varchar(255)"`
	Age      int    `gorm:"type:int"`
}
